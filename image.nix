{ pkgs ? import <nixpkgs> { } }:

pkgs.dockerTools.buildLayeredImage {
    name = builtins.getEnv "IMAGE_NAME";
    tag = builtins.getEnv "IMAGE_TAG";
    contents = [ ./result/bin ];
    config.Cmd = [ "/devlog" ];
}
