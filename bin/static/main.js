const load = () => {
    hljs.highlightAll();

    document.querySelectorAll("#menu, a.nav")
        .forEach(element => element.addEventListener(
            "click", 
            event => {
                console.log(event);
                if (innerWidth < 1080) { 
                    document.querySelector("nav").classList.toggle("show");
                }
            }
        ));
}

load();

addEventListener("htmx:afterSettle", load);
