let () =
    let port = 8080 in
    let interface = "0.0.0.0" in
    Dream.run ~port ~interface
    @@ Dream.logger 
    @@ Dream.router
    Router.routes;;
