open Batteries
open Devlog
open Printf

module StringMap = Map.Make(String)

module Route = struct
    type t =
    | Home
    | Page of string
    | Static of string

    let home = "/"
    let page = "/p"
    let static = "/static"
end

let string_of_route route =
    let open Route in
    match route with
    | Home -> home
    | Page article -> sprintf "%s/%s" page article
    | Static file -> sprintf "%s/%s" static file

let headings =
    let headings md =
        let re = Re.Pcre.regexp "\n\n" in
        Re.split re md
        |> List.filter (flip String.starts_with "## ")
        |> List.map (fun str -> 
                String.replace 
                    ~str 
                    ~sub:"## " 
                    ~by:"" 
                |> Tuple2.second)
    in
    pages
    |> List.filter_map 
        (fun page ->
            let page' =
                String.replace 
                    ~str:page 
                    ~sub:".md" 
                    ~by:"" 
                    |> Tuple2.second
            in
            page' 
            |> open_file 
            |> Option.map headings
            |> Option.map (fun headings -> page', headings))
    |> List.fold
        (fun acc (page, headings) -> StringMap.add page headings acc)
        StringMap.empty



let _nav ?(headings=[]) _path =
    let open Dream_html in
    let open HTML in
    let open Route in
    let element_of_string (current, str) = 
        let headings = 
            let sanitise =
                String.replace_chars (fun c -> 
                    match c with 
                    | ' ' -> "-"
                    | '!' -> String.empty
                    | c -> String.of_char c) 
                % String.lowercase_ascii 
            in
            [ ul [ class_ "headings" ] 
                @@ List.map 
                    (fun heading -> li [] 
                        [ a [ href "#%s" @@ sanitise heading 
                            ; class_ "nav" ] 
                            [txt "%s" heading ] 
                        ])
                    headings
            ] 
        in
        let attr = 
            if current 
            then [ class_ "current" ] 
            else [] 
        in
        [ li attr [ txt ~raw:true "%s" str ]
        ] @ (if current then headings else [])
    in
    let a_of_path path =
        let title = 
            String.replace 
                ~str:path 
                ~sub:".md" 
                ~by:"" 
            |> Tuple2.second
        in
        let path =
            title
            |> (fun path -> Page path)
            |> string_of_route
        in
        if title = _path
        then
            true, 
            strong [] @@ 
                [ txt "%s" title 
                ]
        else false, 
            a   [ href "%s" path 
                ; Hx.boost true 
                ; Hx.target "body"
                ; Hx.swap "innerHTML"
                ; class_ "nav"
                ] 
                [ txt "%s" title ]
    in
    let ctt = 
        pages
        |> List.map 
            ( element_of_string
            % Tuple2.map2 to_string
            % a_of_path)
        |> List.flatten
    in
    [ ul []  ctt ]

let static = Dream.scope Route.static []
    [ Dream.get "/**" 
        @@ Dream.static
            ~loader:(fun _root path _req -> 
                match Static.read path with
                | None -> Dream.empty `Not_Found 
                | Some asset -> Dream.respond asset)
            ""
    ]

let home = Dream.get Route.home (fun req ->
    let open Dream_html in
    let open HTML in
    let title = "" in
    let nav = _nav Route.home in
    htmx_respond ~req ~title ~home:true ~nav 
    @@ p [] [])

let page = Dream.scope Route.page []
    [ Dream.get "/:id" (fun req ->
        let title = Dream.param req "id" in
        match open_file title with
        | None -> Dream.html ~status:`Not_Found ""
        | Some md -> 
                let nav = _nav ~headings:(StringMap.find title headings) title in
                md
                |> Omd.of_string
                |> Omd.to_html
                |> Templates.article ~title
                |> htmx_respond ~req ~title ~nav)
    ]

let routes =
    [ static
    ; home
    ; page
    ];;
