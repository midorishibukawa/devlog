open Batteries

module Templates = Templates

let htmx_respond ~req ~title ~nav ?(home=false) body =
    let body' = match Dream.header req "Hx-Request" with
    | None      ->  Templates.page ~title
    | Some _    -> Templates.body' in
    body
    |> body' ~nav ~home
    |> Dream_html.to_string
    |> Dream.respond

let http_error ?(status=`Bad_Request) message =
    let open Dream_html in
    respond ~status @@ HTML.p [] [ txt "%s" message ]

let pages = Markdown.file_list

let open_file = Markdown.read % flip (^) ".md"

let open_html =
    Option.map (Omd.to_html % Omd.of_string) 
    % open_file
