open Dream_html 
open HTML
open Printf

let default_title = "devlog"

let body' ~nav:nav' ?(home=false) content' =
    let email = "midori@shibukawa.io" in
    let home_attr = if home then [ class_ "home" ] else [] in
    body [] 
        [ header home_attr
            [ a [ href "/" 
                ; Hx.boost true 
                ; Hx.target "body"
                ] 
                [ txt "devlog" ] 
            ; button [ id "menu" ] [ txt ~raw:true "&#9776" ] 
            ]
        ; nav home_attr nav'
        ; main [] 
            [ content' ]
        ; footer [] 
            [ txt "midori shibukawa | " 
            ; a [ href "mailto:%s" email ; target "_blank" ] [ txt "%s" email ]
            ]
        ]


let page ~title:title' ~nav:nav' ?(home=false) content' =
    let title =
        let text =
        if String.length title' = 0 
        then sprintf "%s" default_title
        else sprintf "%s - %s" default_title title' in
        title [] "%s" text
    in
    let meta =
            [ meta [ charset "UTF-8" ]
            ; meta [ http_equiv `x_ua_compatible ; content "ID=edge" ]
            ; meta [ name "viewport" ; content "width=device-width, initial-scale=1.0" ]
            ] 
    in
    let htmx =
        let version = "1.9.11" in
        let integrity_ = "sha384-0gxUXCCR8yv9FM2b+U3FDbsKthCI66oH5IA9fHppQq9DDMHuMauqq1ZHBpJxQ0J0" in
        [ script  
            [ src "https://unpkg.com/htmx.org@%s" version
            ; integrity "%s" integrity_
            ; crossorigin `anonymous 
            ] 
            "" 
        ] 
    in
    let stylesheet = link [ rel "stylesheet" ; href "/static/style.css" ] in
    let hightlightjs =
        let root = "https://cdnjs.cloudflare.com/ajax/libs/highlight.js" in
        let version = "11.9.0" in
        let language_script language =
            script [ src "%s/%s/languages/%s.min.js" root version language ] "" in
        let languages = [ "ocaml" ] |> List.map language_script in
        [ script    [ src "%s/%s/highlight.min.js" root version ] ""
        ; script    [ src "/static/main.js" ; type_ "module" ] ""
        ] 
        @ languages in
    html [ lang "en" ]
        [ head [] 
           ([ title ; stylesheet ]
            @ meta
            @ htmx
            @ hightlightjs)
        ; body' ~nav:nav' ~home content'
        ]

let p text = p [] [ txt "%s" text ]

let article ~title content = 
    article [] 
        [ h1 [] 
            [ txt "%s" title  
            ; txt ~raw:true "%s" content 
            ]
        ]
