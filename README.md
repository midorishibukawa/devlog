# midori's devlog

hi! this is the repository for my devlog webserver. the webserver was written in 
[ocaml](https://ocaml.org) using [dream](https://github.com/aantron/dream) and 
[htmx](https://htmx.org), while the actual content was written in markdown, and 
converted into html with [omd](https://github.com/ocaml/omd). I use nix flakes 
in order to set up both the prd build and dev environments.


## running the web server

in order to run the server, you'll first need to install its dependencies. if you're 
using the [nix](https://nixos.org) package manager, the flake.nix provided (credits 
to [opam-nix](https://github.com/tweag/opam-nix) will setup everything for you 
by simply running the following command:

```
nix develop
```

otherwise, you'll need both dune and opam installed on your system, as well as 
the following opam packages:

```
ocaml batteries dune dream crunch omd
```

once you've got everything you need, you can start the web server by using the following commands:

```
dune build && dune exec devlog 
```

alternatively, you can build and run on watch mode with the following command:

```
dune build -w @run

```
the web server is set up to listen on https://localhost:8080

